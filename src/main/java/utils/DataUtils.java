package utils;

import user.BankAccount;
import user.BankCustomer;
import user.Card;

public class DataUtils {
    public static BankCustomer getCustomer() {
        BankCustomer customer = new BankCustomer(2, "1800000234102", "Ayan", "Precup");
        BankAccount account = new BankAccount("RO00RZBR991105", " RON");
        account.updateBalance(150);
        Card card = new Card("1234624536242485", "3838");
        account.setCard(card);
        customer.setBankAccount(account);
        return customer;
    }
}
