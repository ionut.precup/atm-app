package output;

import ui.Display;
import user.BankAccount;

public class AtmDispatcher {
    public static void withdraw(BankAccount account, int amount) {
        double balance = account.getBalance();
        if (balance < amount) {
            Display.showNotEnoughMoneyMsg();
            return;
        }

        account.updateBalance(balance - amount);
    }

}
