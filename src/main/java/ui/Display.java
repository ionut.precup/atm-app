package ui;

import input.UserInput;
import user.BankAccount;
import user.BankCustomer;
import user.Card;

public class Display {
    public static final String App_MENU =
            """
                  Select an option:
                  1. Withdraw cash
                  2. Check Balance
                  3. Change Pin
                  4. Activate card
                  5. Deposit cash
                  6. Deactivate Account
                  7. Show IBAN
                  8. Show card details
                    """;

    private static String ok = "Free Cash ATM";
    private static final String ATM_NAME = ok;

    public static void showWelcomeMsg() {
        System.out.println("Welcome to " + ATM_NAME);
    }
    public static String askForPin() {
        System.out.println("Please enter your pin number");
        return readFromKeyboard();
    }
        public static String askForNewPin() {
        System.out.println("Please enter your new pin number");
        return readFromKeyboard();
        }
    public static String confirmNewPin() {
        System.out.println("Please confirm your new pin number");
        return readFromKeyboard();
    }

    private static String readFromKeyboard() {
        return UserInput.readfromkeyboard();
    }

    public static void displayInvalidPinMsg() {
        System.out.println("Pin inccorect, please try again");
    }
    public static void lockUserFor24h (){
        System.out.println("Incorrect Pin, Card locked for 24h.");
    }

    public static void showMenu () {
        System.out.println(App_MENU);
    }
    public static int askUserForAmount (){
        System.out.println("Type in amount you want: ");
        return UserInput.readintfromkeyboard();

    }

    public static void showCustomerBalanceSheet(BankAccount bankAccount) {
        System.out.println("Your Balance is: " + bankAccount.getBalance()+ "" + bankAccount.getCurrency());
    }


    public static void showNotEnoughMoneyMsg() {
        System.out.println("Your Bank account does not have enough cash");
    }


    public static void activateCardMessage(BankCustomer someone) {
        System.out.printf("Thank you %s for activating the card",someone.getFullName());
    }

    public static void informUserWithDeactivation() {
        System.out.println("Thank you for being a valuable customer. Hope you come back.");
    }

    public static void showIban(BankCustomer someone) {
        System.out.println("IBAN: " + someone.getBankAccount().getAccountNumber());
    }

    public static void showCardDetails(Card card) {
        System.out.println("Card details:" + card.getCardNumber());
    }
}
