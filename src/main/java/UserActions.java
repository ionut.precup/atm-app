import output.AtmDispatcher;
import ui.Display;
import user.BankCustomer;

public class UserActions {
    static void withdraw(BankCustomer someone) {
        int amount = Display.askUserForAmount();
        AtmDispatcher.withdraw(someone.getBankAccount(), amount);
        Display.showCustomerBalanceSheet(someone.getBankAccount());
    }

    static void changePin(BankCustomer someone) {
        String initialPin = Display.askForPin();
        boolean isValid = someone.validatePin(initialPin);
        if (!isValid) {
            Display.displayInvalidPinMsg();
            return;
        }
        String changedPin = Display.askForNewPin();
        String confirmedChangedPin = Display.confirmNewPin();
        boolean pinMatches = someone.validateChangedPin(changedPin, confirmedChangedPin);
        if (!pinMatches) {
            Display.displayInvalidPinMsg();
            return;
        }
        someone.updatePin(changedPin);
        System.out.println("Pin Correct. Update pin: " + changedPin);
    }

    static void depositCash(BankCustomer someone) {
        int amount = Display.askUserForAmount();
        someone.depositCash(amount);
        Display.showCustomerBalanceSheet(someone.getBankAccount());
    }

    static void deactivateAccount(BankCustomer someone) {
        someone.deactivateAccount();
        Display.informUserWithDeactivation();
    }
}
