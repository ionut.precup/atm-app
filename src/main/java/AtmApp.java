import ui.Display;
import user.BankCustomer;

import static input.UserInput.authenticate;
import static utils.DataUtils.getCustomer;

public class AtmApp {
    public static final String ATM_NAME = "Free cash ATM";

    public static void main(String[] args) {

        BankCustomer someone = getCustomer();

        Display.showWelcomeMsg();

        boolean passThrough = authenticate(someone);
        if (!passThrough) {
            Display.lockUserFor24h();
            return;
        }
        Display.showMenu();
        String option = someone.selectOptionMenu();
        switch (option) {
            case "1" -> UserActions.withdraw(someone);
            case "2" -> Display.showCustomerBalanceSheet(someone.getBankAccount());
            case "3" -> UserActions.changePin(someone);
            case "4" -> Display.activateCardMessage(someone);
            case "5" -> UserActions.depositCash(someone);
            case "6" -> UserActions.deactivateAccount(someone);
            case "7" -> Display.showIban(someone);
            case "8" -> Display.showCardDetails(someone.getBankAccount().getCard());

        }
    }


}